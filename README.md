# hyprland-dotfiles

![](assets/hyprland.png)

## Introduction

My personal dotfiles for hyprland; helps me remember how I did the install!  

Installed Arch using its own `installer` command and the hyprland desktop.

I can't guarantee that the scripts are error free, so worth checking them through first.

There is also a dark nord theme. Keybinding to switch between light and dark mode is `mainMod SHIFT, T` -- it changes waybar, kitty, wofi, thunar.  It is still **work-in-progress**. For example, icons are not changing and has to be done through `nwg-look`.  Also, apart from waybar, there are times when the themes do not change.  However, reloading hyprland seems to work: `$mainMod SHIFT, R`.


## Installing applications

>
> ❗ **NOTE:** The script below does not install Arch hyprland (see **Introduction** above), it just installs the applications needed for my hyprland styles/preferences.
>

```bash
sudo pacman -S git
git clone https://gitlab.com/dajhub/hyprland-dotfiles
cd hyprland-dotfiles
./install.sh
```

## Using Stow to create the symlinks

Before creating the symlinks with **stow** and preventing it from failing you might want to firstly delete two files in .config: *kitty* and *hypr*. In a terminal:

```bash
cd
cd .config
rm -rf kitty hypr
```
To run stow:

```bash
cd hyprland-dotfiles
stow .
```

## Copying across wallpapers

In a terminal:

```bash
cd hyprland-dotfiles
./wallpapers.sh
```
## Connect to Wi-Fi

In a terminal to identify Wi-Fi:

```bash
nmcli dev wifi
```

To connect (c) to identified Wi-Fi:

```bash
nmcli device wifi c <<SSID>> password <<PASSWORD>>
```
