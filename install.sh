#!/bin/bash
echo "###############################################"
echo '# Installing yay...'
echo "###############################################"

cd
sudo pacman -Syy --needed git base-devel && git clone https://aur.archlinux.org/yay.git && cd yay && makepkg -si
yay
cd

echo "###############################################"
echo '# Installing required for hyprland setup...'
echo "###############################################"

yay -S --noconfirm \
         waybar \
         wlogout \
         swaylock-effects \
         swaybg \
         swayidle \
         inter-font \
         otf-san-francisco \
         ttf-nerd-fonts-symbols \
         wl-clipboard \
         brightnessctl \
         swaync \
         grim \
         slurp \
         nwg-look \
         pamixer


echo "###############################################"
echo '# Installing Applications...'
echo "###############################################"

yay -S --noconfirm \
        firefox \
        thunar \
        tumbler \
        xarchiver \
        unzip \
        gvfs \
        fish \
        viewnior \
        htop \
        fastfetch \
        micro \
        atril

 
echo "###############################################"
echo '# Installs stow for symbolic links...'
echo "###############################################"

yay -S --noconfirm stow

echo "###############################################"
echo '# Installing OnlyOffice ...'
echo "###############################################"

yay -S --noconfirm onlyoffice-bin


echo "###############################################"
echo '# Installing Code ...'
echo "###############################################"

yay -S --noconfirm code 




echo "###############################################"
echo '# Printer ...'
echo "###############################################"

yay -S --noconfirm print-manager system-config-printer hplip cups 
sudo systemctl start cups.service
sudo systemctl enable cups.service

echo "###############################################"
echo '# SDDM Astronaut theme ...'
echo "###############################################"
# Install dependencies
yay -S --noconfirm qt6-5compat qt6-declarative qt6-svg

# Install astronaut theme
sudo git clone https://github.com/keyitdev/sddm-astronaut-theme.git /usr/share/sddm/themes/sddm-astronaut-theme
sudo cp /usr/share/sddm/themes/sddm-astronaut-theme/Fonts/* /usr/share/fonts/

# Makes it the default theme
echo "[Theme]
Current=sddm-astronaut-theme" | sudo tee /etc/sddm.conf

echo "###############################################"
echo '# Set fish as default shell ...'
echo "###############################################"

chsh -s /usr/bin/fish




echo "###################################################"
echo '# Finally, need to run the script to copy files ...'
echo "###################################################"



