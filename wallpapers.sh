echo "###############################################"
echo '# Copying Wallpapers across ...'
echo "###############################################"

cd
mkdir -p Pictures
git clone https://gitlab.com/dajhub/wallpapers
cd wallpapers
cp -r ~/wallpapers ~/Pictures/
cd
cd Pictures/wallpapers
rm README.md
cd
rm -rf wallpapers


echo "###############################################"
echo '# Finished...'
echo "###############################################"
