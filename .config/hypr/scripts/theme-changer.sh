#!/bin/bash

# ------ Changes Waybar ------

mv ~/.config/waybar/style.css ~/.config/waybar/alt-theme/style2.css

mv ~/.config/waybar/alt-theme/style.css ~/.config/waybar/style.css

mv ~/.config/waybar/alt-theme/style2.css ~/.config/waybar/alt-theme/style.css 

killall waybar && waybar

sleep 2.0

# ------ Changes wofi ------


if grep -q "Nord Dark Theme" ~/.config/waybar/style.css; then
  sed -i 's/nord-light/nord-dark/g' ~/.config/wofi/style.css
else
  sed -i 's/nord-dark/nord-light/g' ~/.config/wofi/style.css
fi


# ------ Changes kitty ------

if grep -q "Nord Dark Theme" ~/.config/waybar/style.css; then
  sed -i 's/nord-light/nord-dark/g' ~/.config/kitty/kitty.conf
else
  sed -i 's/nord-dark/nord-light/g' ~/.config/kitty/kitty.conf
fi

sleep 1.0
kill -SIGUSR1 $KITTY_PID

# ------ Changes Thunar ------

pkill -f thunar 

if grep -q "Nord Dark Theme" ~/.config/waybar/style.css; then
  gsettings set org.gnome.desktop.interface gtk-theme Nordic
else
  gsettings set org.gnome.desktop.interface gtk-theme Orchis-Light-Compact-Nord
fi



# ------ Changes icons for gtk ------

if grep -q "Nord Dark Theme" ~/.config/waybar/style.css; then
  sed -i 's/Papirus-Light/Papirus-Dark/g' ~/.config/gtk-3.0/settings.ini
else
  sed -i 's/Papirus-Dark/Papirus-Light/g' ~/.config/gtk-3.0/settings.ini
fi

gtk-update-icon-cache

sleep 3.0
hyprctl reload



