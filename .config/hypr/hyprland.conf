
# ------ Monitors -------

# See https://wiki.hyprland.org/Configuring/Monitors/

#Lenova laptop
monitor=eDP-1, 1920x1080, 0x0, 1
monitor=HDMI-1, 1920x1080, 1920x0, 1

#Toshiba laptop
#monitor=LVDS-1, 1366x768, 0x0, 1
#monitor=HDMI-1, 1920x1080, 1366x0, 1

# ------ My Programs
$terminal = kitty
$fileManager = dolphin

# ------ Execute apps at launch ------

exec-once = waybar 
exec-once = ~/.config/hypr/scripts/background.sh
exec-once = wl-paste --watch cliphist store 
exec-once = swaync -conf .config/swaync/config.json
exec-once = /usr/lib/polkit-kde-authentication-agent-1 # authentication dialogue for GUI apps
exec-once = swayidle -w timeout 300 'swaylock -f' timeout 600 'sleep' before-sleep 'swaylock -f' 
#exec-once = ~/.config/hypr/scripts/suspend.sh 

# ------ Key bindings ------

source = ~/.config/hypr/bind.conf



# ------ Variables ------

env = XCURSOR_SIZE,2



# ------ Input: keyboard, touchpad ------
input {
    kb_layout = gb
    kb_variant =
    kb_model =
    kb_options =
    kb_rules =

    numlock_by_default = true

    follow_mouse = 1

    touchpad {
        disable_while_typing = true
        natural_scroll = true
        disable_while_typing = true

    }

    sensitivity = 0 # -1.0 - 1.0, 0 means no modification.
}

# ------ Miscellaneous ------

misc {
    disable_hyprland_logo = true
    disable_splash_rendering = true
    focus_on_activate = true
}

# ------ General ------

general {
    # See https://wiki.hyprland.org/Configuring/Variables/ for more

    gaps_in = 5
    gaps_out = 10
    border_size = 2
    col.active_border = 0xffcba6f7
    col.inactive_border = 0xff313244
    no_border_on_floating = true
    layout = dwindle
}

# ------ Decorations ------

decoration {
    # See https://wiki.hyprland.org/Configuring/Variables/ for more

    rounding = 0
    active_opacity = 1.0
    inactive_opacity = 1.0
    blur {
        enabled = yes
        size = 10
        passes = 1
        new_optimizations = on
    }
    drop_shadow = yes
    shadow_ignore_window = true
    shadow_range = 4
    shadow_offset = 2 2 
    shadow_render_power = 2
    col.shadow= 0x66000000
}

# ------ Animations ------

animations {
    enabled = true

    
    bezier = myBezier, 0.05, 0.9, 0.1, 1.05

    animation = windows, 1, 7, myBezier
    animation = windowsOut, 1, 7, default, popin 80%
    animation = border, 1, 10, default
    animation = borderangle, 1, 8, default
    animation = fade, 1, 7, default
    animation = workspaces, 1, 6, default
}

dwindle {
    pseudotile = true # master switch for pseudotiling. Enabling is boun>
    preserve_split = true # you probably want this
}

#master {
#    new_is_master = true
#}

gestures {
    workspace_swipe = true
}

general {
    col.active_border = rgb(7fbbb3) # rgb(6272a4) # or rgb(44475a)
    col.inactive_border = rgb(2d353b)
}

decoration {
    col.shadow = rgba(1E202966)
}

# ------ Workspace settings ------

workspace=LVDS-1,2 

# ------ Window rules ------

windowrule = float, file_progress
windowrule = float, confirm
windowrule = float, dialog
windowrule = float, download
windowrule = float, notification
windowrule = float, error
windowrule = float, splash
windowrule = float, confirmreset
windowrule = float, title:Open File
windowrule = float, title:branchdialog
windowrule = float, zoom

#windowrule=workspace 1, kitty
windowrule=workspace 1, konsole
windowrule=workspace 2, thunar
windowrule=workspace 3, code
windowrule=workspace 3, kate
windowrule=workspace 4, ONLYOFFICE
windowrule=workspace 5, firefox
windowrule=workspace 5, Brave
windowrule=workspace 6, virt-manager
windowrule=workspace 7, Joplin
windowrule=workspace 8, Gimp|Gimp-2.10
windowrule=workspace 8, Inkscape

windowrule = float, kitty
windowrule = float, Lxappearance
windowrule = float, nwg-look
windowrule = float, atril
windowrule = float, ncmpcpp
windowrule = float, catfish
windowrule = float, Viewnior
windowrule = float, pavucontrol-qt
windowrule = float, gnome-font
windowrule = float, org.gnome.Settings
windowrule = float, xarchiver
windowrule = float, wdisplays
windowrule = float, *.exe
windowrule = fullscreen, wlogout
windowrule = float, title:wlogout
windowrule = fullscreen, title:wlogout
windowrule = idleinhibit focus, mpv
windowrule = float, title:^(Firefox — Sharing Indicator)$
windowrule = move 0 0, title:^(Firefox — Sharing Indicator)$th
windowrulev2 = float,class:^(firefox)$,title:^(Picture-in-Picture)$
windowrulev2 = float,class:^(firefox)$,title:^(Library)$
windowrule = float, title:^(About Mozilla Firefox)$

windowrule = size 800 600, title:^(Volume Control)$
#windowrule = size 800 600, title:^(Save As)$
windowrule = move 75 44%, title:^(Volume Control)$
windowrule = move 1100 44%, title:^(kitty)$

windowrulev2 = opacity 0.95, title:^(micro ~)$,class:^(kitty)$ 

# ------ Window size ------

windowrulev2 = size 800 600, title:^(Catfish)$
#windowrulev2 = size 800 600, title:^(kitty)$

#windowrulev2 = float, class:floating


windowrulev2 = size 600X600,title:Locate ISO media
windowrulev2 = center,title:Locate ISO media

# ------ Dialogues ------

windowrule=float,title:^(Open File)(.*)$
windowrule=float,title:^(Select a File)(.*)$
windowrule=float,title:^(Open Folder)(.*)$
windowrule=float,title:^(Save As)(.*)$
windowrule=float,title:^(Library)(.*)$


# ------Special Rules for Steam to get the drop-down and lower right “Add Game” menus to work right ------

windowrulev2 = stayfocused, title:^()$,class:^(steam)$
windowrulev2 = minsize 1 1, title:^()$,class:^(steam)$




